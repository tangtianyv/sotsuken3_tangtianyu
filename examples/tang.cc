#include "ns3/core-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/wifi-module.h"
#include "ns3/mobility-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/wifi-80211p-helper.h"
#include "ns3/wave-mac-helper.h"
#include "ns3/internet-stack-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/ipv4-interface-container.h"
#include "ns3/v4ping-helper.h"
#include "ns3/netanim-module.h"
#include "ns3/tty-module.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <string>

#include <time.h> 

NS_LOG_COMPONENT_DEFINE ("ttyProtocolMinimum");
using namespace ns3;


Ptr<ConstantVelocityMobilityModel> cvmm;

int 
main (int argc, char *argv[])
{
  bool verbose = true;
  uint32_t nCsma = 16;
  uint32_t nWifi = 2;
  uint32_t range =100;
  uint32_t step  =200;
  //bool tracing = false;

  CommandLine cmd;
  //cmd.AddValue ("nCsma", "Number of \"extra\" CSMA nodes/devices", nCsma);
  //cmd.AddValue ("nWifi", "Number of wifi STA devices", nWifi);
  //cmd.AddValue ("verbose", "Tell echo applications to log if true", verbose);
  //cmd.AddValue ("tracing", "Enable pcap tracing", tracing);

  cmd.Parse (argc,argv);

  if (nWifi > 18)
    {
      std::cout << "nWifi should be 18 or less; otherwise grid layout exceeds the bounding box" << std::endl;
      return 1;
    }

  if (verbose)
    {
      LogComponentEnable ("UdpEchoClientApplication", LOG_LEVEL_INFO);
      LogComponentEnable ("UdpEchoServerApplication", LOG_LEVEL_INFO);
    }


  //Create csma 
  NodeContainer csmaNodes;
  csmaNodes.Create (nCsma);
 
  CsmaHelper csma;
  csma.SetChannelAttribute ("DataRate", StringValue ("100Mbps"));
  csma.SetChannelAttribute ("Delay", TimeValue (NanoSeconds (6560)));
  NetDeviceContainer csmaDevices;
  csmaDevices = csma.Install (csmaNodes);
  std::cout<<"csma created\n";

  //create wifinode
  NodeContainer wifiStaNodes;
  wifiStaNodes.Create (nWifi);
  std::cout<<"wifi created\n";

  //create channel
  YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default ();
  YansWifiChannelHelper wifiChannel= YansWifiChannelHelper::Default ();
  wifiChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
  wifiChannel.AddPropagationLoss("ns3::RangePropagationLossModel", "MaxRange", DoubleValue(range));
  Ptr<YansWifiChannel> channel = wifiChannel.Create ();
  wifiPhy.SetChannel (channel); //创建通道对象并把他关联到物理层对象管理器
  wifiPhy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11);
  NqosWaveMacHelper wifi80211pMac = NqosWaveMacHelper::Default ();
  Wifi80211pHelper wifi80211p = Wifi80211pHelper::Default ();
  wifi80211p.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                                      "DataMode",StringValue ("OfdmRate6MbpsBW10MHz"),
                                      "ControlMode",StringValue ("OfdmRate6MbpsBW10MHz"));
  NetDeviceContainer wifidevices = wifi80211p.Install (wifiPhy, wifi80211pMac, csmaNodes);
  wifidevices.Add(wifi80211p.Install (wifiPhy, wifi80211pMac, wifiStaNodes));
  std::cout<<"device created\n";

  MobilityHelper mobility;
  //car
  mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
  mobility.Install(wifiStaNodes);
  Ptr<ConstantPositionMobilityModel> cpmm;
  cpmm = wifiStaNodes.Get(0)->GetObject<ConstantPositionMobilityModel> ();
  cpmm->SetPosition(Vector(250,350,0));
  std::cout<<"mobiw1 created\n";

  mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
  mobility.Install(wifiStaNodes.Get(1));
  cpmm = wifiStaNodes.Get(1)->GetObject<ConstantPositionMobilityModel> ();
  cpmm->SetPosition(Vector(240,600,0));
  std::cout<<"mobiw2 created\n";

  mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                 "MinX", DoubleValue (80.0),
                                 "MinY", DoubleValue (40.0),
                                 "DeltaX", DoubleValue (step),
                                 "DeltaY", DoubleValue (step),
                                 "GridWidth", UintegerValue (4),
                                 "LayoutType", StringValue ("RowFirst"));

  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (csmaNodes);
  std::cout<<"mobi created\n";

  ttyHelper ttyProtocol;
  
  Ipv4ListRoutingHelper listrouting;
  listrouting.Add(ttyProtocol, 10);

  InternetStackHelper internet;
  internet.SetRoutingHelper(listrouting);
  internet.Install (csmaNodes);
  internet.Install (wifiStaNodes);
  std::cout<<"stack created\n";
  Ipv4AddressHelper ipv4;

  //  IP
  ipv4.SetBase ("10.1.2.0", "255.255.255.0");
  Ipv4InterfaceContainer csmaInterfaces;
  csmaInterfaces = address.Assign (csmaDevices);

  ipv4.SetBase ("10.1.3.0", "255.255.255.0");
  ipv4.Assign (wifidevices);
  std::cout<<"add created\n";
  Simulator::Stop(Second(6));
  AnimationInterface anim ("tang.xml");
  Simulator::Run ();
  Simulator::Destroy ();
  return 0;
}
