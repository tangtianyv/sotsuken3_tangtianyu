## -*- Mode: python; py-indent-offset: 4; indent-tabs-mode: nil; coding: utf-8; -*-

def build(bld):
    module = bld.create_ns3_module('tty', ['internet', 'wifi'])
    module.includes = '.'
    module.source = [
        'model/tty-packet.cc',
        'model/tty-routing-protocol.cc',
        'helper/tty-helper.cc',
        ]

    #tty_test = bld.create_ns3_module_test_library('tty')
   # tty_test.source = [
      #  'test/tty-id-cache-test-suite.cc',
       # 'test/tty-test-suite.cc',
       # 'test/tty-regression.cc',
       # 'test/bug-772.cc',
       # 'test/loopback.cc',
       # ]

    headers = bld(features='ns3header')
    headers.module = 'tty'
    headers.source = [
        'model/tty-packet.h',
        'model/tty-routing-protocol.h',
        'helper/tty-helper.h',
        ]

    #if bld.env['ENABLE_EXAMPLES']:
     #   bld.recurse('examples')

   # bld.ns3_python_bindings()
